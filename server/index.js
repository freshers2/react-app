const express =require('express')
const dotenv=require('dotenv').config()
const mongoose=require("mongoose")
const cors=require("cors")
const postRoutes=require("./router/postRouter.js")
const app=express()
const port=process.env.port

app.use(cors())
app.use(express.json())
app.use("/post",postRoutes)

const url='mongodb://localhost:27017/OneToOneHelp'
mongoose.connect(url,{useNewUrlParser:true})
mongoose.connection.on('connected',()=>{
    console.log("mongodb successfully connected")
})
mongoose.connection.on('error',()=>{
    console.log("error to connect mongodb")
})
// mongoose.set('useFindAndModify', false)
app.listen(port,()=>{
    console.log(`server running in ${port} sucessfully`)
})
