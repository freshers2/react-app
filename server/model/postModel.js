const { timeStamp } = require('console')
const mongoose=require('mongoose')


const postSchema=mongoose.Schema({
    title:String,
    message:String,
    creator:String,
    tags:[String],
    selectedFile:String,
    likeCount:{
        type:Number,
    default:0    },

},timeStamp());

module.exports=mongoose.model("postmessage",postSchema)