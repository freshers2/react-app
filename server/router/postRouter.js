const express=require("express")

const router=express.Router()
const postCntrl=require("../controller/postController")


router.route("/").get(postCntrl.getPosts).post(postCntrl.createPost)


module.exports=router;
