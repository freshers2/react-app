
const postMessage=require("../model/postModel")

const getPosts=async(req,res)=>{
   try{

    let postMessages=await postMessage.find()
    res.json(postMessages)
   }catch(error){
       console.log(error.message)
   }
}

const createPost=async (req,res)=>{
   const  body=req.body;
   console.log({body})
    try{
       const newPost=await new postMessage(body)
       await newPost.save()
       res.json(newPost)
    }catch(e){
        console.log(e.message)
    }

}

module.exports={getPosts,createPost}